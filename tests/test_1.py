import pytest
import requests


def test_1():

    url = "https://the-internet.herokuapp.com/context_menu"
    response = requests.get(url)
    assert response.status_code == 200, "URL ERROR"
    
    pageText = response.text
    testStr = "Right-click in the box below to see one called 'the-internet'"
    assert(testStr in pageText), "The string is not found on the page"
